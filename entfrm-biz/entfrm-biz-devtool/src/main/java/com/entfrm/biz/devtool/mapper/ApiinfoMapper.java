package com.entfrm.biz.devtool.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entfrm.biz.devtool.entity.Apiinfo;

/**
 * @author entfrm
 * @date 2020-04-24 22:18:00
 * @description 接口Mapper接口
 */
public interface ApiinfoMapper extends BaseMapper<Apiinfo> {

}
